import {  NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrandComponent } from './brand/brand.component';
import { FormComponent } from './form/form.component';
import { OnboardComponent } from './onboard/onboard.component';
import { RegisterComponent } from './register/register.component';


const routes: Routes = [

  {path : 'Bank-Detials', component: FormComponent },
  {path : 'register', component: RegisterComponent },
  {path : 'Brand', component: BrandComponent },
  { path : 'onBoard' , component : OnboardComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
