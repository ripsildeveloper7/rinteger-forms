import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { brand } from './brandlogo';

@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.css']
})
export class BrandComponent implements OnInit {
registerForm : FormGroup;
brand : brand;

trademarkcertificate = new FormControl('', [Validators.required]);
brandlogo = new FormControl('',[Validators.required]);
acknowledment = new FormControl('',[Validators.required]);
brandpacking = new FormControl('',[Validators.required]);
brandvisible = new FormControl('',[Validators.required]);
  reader: FileReader;
  url: any[];
  constructor( private cd : ChangeDetectorRef) { }

  ngOnInit() {
  }

  onClick(){
    this.brand = new brand();
    this.brand.Trademarkcertificate = this.trademarkcertificate.value;
    this.brand.brandLogo = this.brandlogo.value;
    this.brand.Acknowledgement = this.acknowledment.value;
    this.brand.brandPacking = this.brandpacking.value;
    this.brand.brandVisible = this.brandvisible.value;
    localStorage.setItem('Brand-Register',JSON.stringify(this.brand ));
    let formValue = JSON.parse(localStorage.getItem('this.regForm'));
   console.log( localStorage.setItem);
   /*  console.log( this.getData); 
    console.log(formValue);
    console.log(this.userID); */
  }


  /* onOpen(event,field){
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
  
      if(!file.type.startsWith('image')){
        this.registerForm.get(field).setErrors({
          required : true
        });
          this.cd.markForCheck();
      }else {
        this.registerForm.patchValue({
          [field] : file
        });
        this.cd.markForCheck();
      }
    }
  } */

  onOpen(images: any){
    const fileUpload = images;
this.url = [];
const files = images;
if(files){
  for(const file of files){
    this.reader = new FileReader();
    this.reader.onload= (e:any) => {
    this.url.push(e.target.result);
    }
    this.reader.readAsDataURL(file)
  }
 }
}

}
