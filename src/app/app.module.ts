import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormComponent } from './form/form.component';
import { RouterModule } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { BrandComponent } from './brand/brand.component';
import { OnboardComponent } from './onboard/onboard.component';

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    RegisterComponent,
    BrandComponent,
    OnboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule
  ],

  exports : [FormComponent,RegisterComponent,BrandComponent],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
