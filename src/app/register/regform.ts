export class regform { 
    
    GST : string;
    panCard : string;
    AddressProof : string;
    GroceryProduct
    CancelledCheque : string;
    OfficeAddressProof : string;
    Email : string;
    digitalSign : string;
    sellingProduct : number;
    CustomerType : string;
    AnnualTurnOver : number;
    Product : number;
    desc : string;
}