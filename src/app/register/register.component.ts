import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { SignIn } from './appform';
import { regform } from './regform';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  form : FormGroup;
  Data  = [
    {'name ': 'aaa', 'value' : 'Manufactuer'},
    {'name' : 'bbb', 'value' : 'Retailer'},
    {'name' : 'ccc', 'value' : 'Wholesale'},
    {'name' : 'ddd', 'value' : 'Other'},
  ];

  value = [
    {'value' : 1, 'alk' : 'Clothing'},
    {'value' : 2, 'alk' : 'HomeAppliences'},
    {'value' : 3, 'alk' : 'others'}
  ]

  range = [
    {'price' : 100},
    {'price' : 500},
    {'price' : 1000},
    {'price' : 'others'},

  ]

 /*  EmployeeNumber = new FormControl ('',[Validators.required])
  EmployeeDepartment = new FormControl('', [Validators.required]);
  EmployeeName = new FormControl('', [Validators.required]);
  EmployeeTask = new FormControl('', [Validators.required]); */
  signinForm : FormGroup;
  userID : SignIn;
  regForm : regform;
  getData : any = [];

  registerForm : FormGroup;
  
    GST = new FormControl ('', [Validators.required]);
    panCard = new FormControl ('',[Validators.required]);
    AddressProof =  new FormControl ('', [Validators.required]);
    groceryProduct = new FormControl ('',[Validators.required]);
    CancelledCheque =  new FormControl ('', [Validators.required]);
    OfficeAddressProof =  new FormControl ('', [Validators.required]);
    Email = new FormControl ('', [Validators.required,Validators.email,Validators.maxLength(25),
      Validators.pattern( "/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/"),Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]);
    digitalSign =  new FormControl ('', [Validators.required]);
    sellingProduct = new FormControl ('', [Validators.required]);
    CustomerType = new FormControl ('', [Validators.required]);
    AnnualTurnOver = new FormControl ('', [Validators.required,Validators.minLength(20)]);
    Product = new FormControl ('', [Validators.required]);
  desc = new FormControl ('', [Validators.required]) 

  public slideVal: number = 1;
  fileUpload: any;
  urls = new Array<string>();
  reader: FileReader = new FileReader();

  constructor( private cd: ChangeDetectorRef, private fb : FormBuilder){
    this.form = this.fb.group({
      checkArray : this.fb.array([],Validators.required)
    })
  }
  ngOnInit(): void {}
  title = 'forms';

  onCheckBoxChange(e){  
    const checkArray : FormArray = this.form.get('checkArray') as FormArray
    if(e.target.checked) {
      checkArray.push(new FormControl(e.target.value))
    } else{
      let i : number = 0;
      checkArray.controls.forEach((item : FormControl) => {
        if (item.value === e.target.value){
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }



onClick(){
  this.regForm = new regform();
  this.getData = new Array();
 /*  this.userID.subscribe(data => this.getData = data) */
 /* this.regForm = this.fb.group({ */
 this.regForm.GST = this.GST.value
  this.regForm.panCard = this.panCard.value;
  this.regForm.AddressProof = this.AddressProof.value;
  this.regForm.GroceryProduct = this.groceryProduct.value;
  this.regForm.CancelledCheque = this.CancelledCheque.value;
  this.regForm.OfficeAddressProof = this.OfficeAddressProof.value;
  this.regForm.Email = this.Email.value;
  this.regForm.digitalSign = this.digitalSign.value;
  this.regForm.sellingProduct = this.sellingProduct.value;
  this.regForm.CustomerType = this.CustomerType.value;
  this.regForm.AnnualTurnOver = this.AnnualTurnOver.value;
  this.regForm.Product = this.Product.value;
  this.regForm.desc = this.desc.value;
  this.getData = localStorage.setItem('Registraction',JSON.stringify(this.regForm ));
   let formValue = JSON.parse(localStorage.getItem('this.regForm'));
  console.log( localStorage.setItem);
   console.log( this.getData); 
   console.log(formValue);
   console.log(this.userID);

}

/* localStorage.setItem('form-data', JSON.stringify(this.testform.value));

let formValue = JSON.parse(localStorage.getItem('form-data')) */
 /* onClick(input)
{
  var key = "input-" + input;

  var storedValue = localStorage.getItem(key);

  if (storedValue)
      input.value = storedValue;

  input.addEventListener('input', function ()
  {
      localStorage.setItem(key, input.value);
  });
}
 */
/* onOpen(event,field){
  if(event.target.files && event.target.files.length) {
    const [file] = event.target.files;

    if(!file.type.startsWith('image')){
      this.registerForm.get(field).setErrors({
        required : true
      });
        this.cd.markForCheck();
    }else {
      this.registerForm.patchValue({
        [field] : file
      });
      this.cd.markForCheck();
    }
  }
} */

onPrice(val){
  for(let i = 0; i< this.range.length; i++){
    if(val.target.defaultValue == this.range[i].price){
      this.Product.setValue(val.target.defaultValue)
    }
  }
}

onChange(e){
for(let i = 0; i < this.Data.length; i++){
  if(e.target.value === this.Data[i].value){
    this.CustomerType.setValue(e.target.value);
  }
}
}

onPass(e){
  for(let i =0; i < this.value.length; i++){
    if(e.target.defaultValue === this.value[i].alk){
      this.sellingProduct.setValue(e.target.defaultValue);
    }
  }
}

onOpen(images : any){
    this.fileUpload = images;
    this.urls = [];
    const files = images;
    if(files) {
      for(const file of files){
        this.reader = new FileReader();
        this.reader.onload = (e:any) => {
          this.urls.push(e.target.result)
        }
        this.reader.readAsDataURL(file);
      }
    } 
  }
}


