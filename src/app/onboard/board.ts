export class OnBoard {
  

    CompanyName : string;
    CompanyAddress : string;
    GST : string;
    BankDetails : string;
    Email : string;
    BrandName : string;
    Logo : string;
    Desc : string;
    Insta_Acc : string;
    Insta_Id : string;
    FaceBook_Acc : string;
    FaceBook_Id : string;
    Website : string;
    Adminpanel : string;
    productImage : string;
    PhotoShoot : string;
    ProductVideo : string;
    PrimaryContact : string;
    BrandReg : string;
    SpecialCmmt : string;
  getData: any;
}