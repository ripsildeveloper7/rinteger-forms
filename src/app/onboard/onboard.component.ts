import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { OnBoard } from './board';

@Component({
  selector: 'app-onboard',
  templateUrl: './onboard.component.html',
  styleUrls: ['./onboard.component.css']
})
export class OnboardComponent implements OnInit {
  /* OnBoarding : FormGroup; */
  boarding: OnBoard;
  onBoard : FormGroup;
  /* getData : []; */

  value = [
    {'set' : 1, 'type' : 'yes'},
    {'set' : 2, 'type' : 'no'},
    {'set' : 3, 'type' : 'others'},
  ]
  reader: FileReader = new FileReader();
  urls = new Array<string>();
  fileUpload: any;
 
 
  constructor( private fb : FormBuilder, private cd : ChangeDetectorRef) { }

  ngOnInit() {
    this.onClick();
   
  }

  onSubmit(){
   var set = new Array();
    this.boarding = new OnBoard();
   var getData = [
    this.boarding.CompanyName = this.onBoard.controls.companyName.value,
    this.boarding.CompanyAddress = this.onBoard.controls.companyName.value,
    this.boarding.GST = this.onBoard.controls.gst.value,
    this.boarding.BankDetails = this.onBoard.controls.bankDetails.value,
    this.boarding.Email = this.onBoard.controls.email.value,
    this.boarding.BrandName = this.onBoard.controls.brandName.value,
    this.boarding.Logo = this.onBoard.controls.logo.value,
    this.boarding.Desc = this.onBoard.controls.description.value,
    this.boarding.Insta_Acc = this.onBoard.controls.insta_Acc.value,
    this.boarding.Insta_Id = this.onBoard.controls.insta_Id.value,
    this.boarding.FaceBook_Acc = this.onBoard.controls.faceBook_Acc.value,
    this.boarding.FaceBook_Id = this.onBoard.controls.faceBook_Id.value,
    this.boarding.Website = this.onBoard.controls.website.value,
    this.boarding.Adminpanel = this.onBoard.controls.adminPanel.value,
    this.boarding.productImage = this.onBoard.controls.productImage.value,
    this.boarding.PhotoShoot = this.onBoard.controls.photoShoot.value,
    this.boarding.ProductVideo = this.onBoard.controls.productVideo.value,
    this.boarding.PrimaryContact = this.onBoard.controls.primaryContact.value,
    this.boarding.BrandReg = this.onBoard.controls.brandReg.value,
    this.boarding.SpecialCmmt = this.onBoard.controls.specialCmmt.value, ]; 

   var sol = set.push(this.boarding);
     localStorage.setItem('Boarding',JSON.stringify(set)) 
   /*  let formValue = JSON.parse(localStorage.getItem('this.onBoard')); */
    
   console.log( this.boarding); 
   console.log(sol);

  }

  onClick(){
  
    this.onBoard = this.fb.group({
   companyName : ['',Validators.required],
   companyAddress : ['',Validators.required],
   gst : ['',Validators.required],
   bankDetails:['',Validators.required],
   email: ['',Validators.required],
   brandName: ['',Validators.required],
   logo: ['',Validators.required],
   description: ['',Validators.required],
   insta_Acc: ['',Validators.required],
   insta_Id: ['',Validators.required],
   faceBook_Acc: ['',Validators.required],
   faceBook_Id: ['',Validators.required],
   website: ['',Validators.required],
   adminPanel: ['',Validators.required],
   productImage: ['',Validators.required],
   photoShoot: ['',Validators.required],
   productVideo: ['',Validators.required],
   primaryContact: ['',Validators.required],
   brandReg: ['',Validators.required],
   specialCmmt: ['',Validators.required],
  })
  }

 

  AsChange(e,value){
for(let i=0; i<this.value.length;i++){
  switch(e.target.Value === this.value[i].type){
    case(this.boarding.ProductVideo === e.target.value) : {
          this.onBoard.controls[value].setValue(e.target.defaultValue);
          break; }
   case(this.boarding.PhotoShoot === e.target.value)  : {
          this.onBoard.controls[value].setValue(e.target.defaultValue);
          break;}
   case(this.boarding.productImage === e.target.value)  : {
            this.onBoard.controls[value].setValue(e.target.defaultValue);
          break;}
   case(this.boarding.Website === e.target.value)  : {
              this.onBoard.controls[value].setValue(e.target.defaultValue);
          break;}
   case(this.boarding.FaceBook_Acc === e.target.value)  : {
                this.onBoard.controls[value].setValue(e.target.defaultValue);
          break;}
   case(this.boarding.Insta_Acc === e.target.value)  : {
            this.onBoard.controls[value].setValue(e.target.defaultValue);
      break;}
  }
}
  }

  inputImage(images : any){
    this.fileUpload = images;
    this.urls = [];
    const files = images;
    if(files) {
      for(const file of files){
        this.reader = new FileReader();
        this.reader.onload = (e:any) => {
          this.urls.push(e.target.result)
        }
        this.reader.readAsDataURL(file);
      }
    } 
  }
  getReset(){
    this.onBoard.reset();
  }
}
