import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { bank } from './bankform';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  isBank : bank;
  regForm : FormGroup;
  Accountnumber = new FormControl('',[Validators.required]);
  IFSCcode = new FormControl('',[Validators.required]);
  Accountholdername = new FormControl('',[Validators.required]);
  Accounttype = new FormControl('',[Validators.required]);
  Bankname = new FormControl('',[Validators.required]);
  constructor() { }

  ngOnInit() {}

  onChange(){
    this.isBank = new bank();

    this.isBank.AccountNumber = this.Accountnumber.value;
    this.isBank.IFSCcode = this.IFSCcode.value;
    this.isBank.AccountHolderName = this.Accountholdername.value;
    this.isBank.AccountType = this.Accounttype.value;
    this.isBank.BankName = this.Bankname.value;
    localStorage.setItem('Bank-Details',JSON.stringify(this.isBank));
   let formValue = JSON.parse(localStorage.getItem('this.regForm'));
   console.log( localStorage.setItem);
   /* console.log( this.getData); 
   console.log(formValue);
   console.log(this.userID); */
  }

}
